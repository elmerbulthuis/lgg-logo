SHELL:=$(PREFIX)/bin/sh

build: \
	out/lgg-logo-800.png \
	out/lgg-logo-400.png \
	out/lgg-logo-100.png \
	out/lgg-logo-text-800.png \
	out/lgg-logo-text-400.png \
	out/lgg-logo-text-100.png \

rebuild: clean build

clean:
	rm -rf out


out/lgg-logo-800.png: src/Eagle_Illustration.png
	@mkdir --parents $(@D)
	convert $<[0] -resize 800x800 $@

out/lgg-logo-400.png: src/Eagle_Illustration.png
	@mkdir --parents $(@D)
	convert $<[0] -resize 400x400 $@

out/lgg-logo-100.png: src/Eagle_Illustration.png
	@mkdir --parents $(@D)
	convert $<[0] -resize 100x100 $@

out/lgg-logo-text-800.png: src/Eagle.png
	@mkdir --parents $(@D)
	convert $<[0] -resize 800x800 $@

out/lgg-logo-text-400.png: src/Eagle.png
	@mkdir --parents $(@D)
	convert $<[0] -resize 400x400 $@

out/lgg-logo-text-100.png: src/Eagle.png
	@mkdir --parents $(@D)
	convert $<[0] -resize 100x100 $@

.PHONY: \
	build \
	rebuild \
	clean \
